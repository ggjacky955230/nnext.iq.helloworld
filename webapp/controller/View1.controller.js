sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/core/UIComponent"
	
], function(Controller,MessageToast,UIComponent) {
	"use strict";

	return Controller.extend("nnext.iq.HelloWorld.controller.View1", {
		onItemPress: function(oEvent){
				//var sMsg = oEvent.getSource().getModel("i18n").getResourceBundle().getText("helloWorld");
				var sMsg =  oEvent.getSource().getText();
                //var oItem = oEvent.getSource();
				MessageToast.show(sMsg);
             }
	});
});